#
# A sample HEM App to demo parsing of HEM data line by line

# Author: Ashray Manur

import module.hemParsingData 


def main():

	#First open the file and put it into read mode
	#For this exercise this file only has one line 

  fh = open('data/data.csv', 'r')

	#Now iterate through the file line by line 
	#for enumerate() see https://docs.python.org/3/library/functions.html#enumerate

  #A line in the file might look like this
  #VOLT:13.50,NODE:0,DATE:12/10/17-6:57:30 - this line gives you voltage for node 0

  for i, line in enumerate(fh):
    
    parsedObj = module.hemParsingData.lineParser(line)
    #Calling this function each time a line has to be parsed 

    #Now the results of the parsing are stored in parsedObj which is a dict
    # You can extract the values by doing this 

    print parsedObj['type'] #VOLT
    print parsedObj['value'] #13.65
    print parsedObj['node'] #0
    print parsedObj['date'] # 2017-12-08 02:28:40

    #now you can apply logic to this data and repeat the process


   
if __name__ == "__main__":
  main()
