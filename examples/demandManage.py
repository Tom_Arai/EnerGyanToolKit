#
# A sample HEM App to do online demand management
# Pulls data from HEMApp and if the power of the loads crosses 
# a certain threshold, non-priority loads are shut-off

# Author: Ashray Manur

import datetime
import threading
import time
import sys

sys.path.insert(1,'../')

from module.hemSuperClient import HemSuperClient

hemSuperClient = HemSuperClient("localhost", 9931)


#This is function which gets triggered whenever you get data from server
#You can add more logic here for post processing
def update(message, address):
	print message
	
	#check if the message is recieved contains power data
	if(message['TYPE'] == 'ACPOWERACTIVE'):
		onlineDemandManagement(message['VALUE'])

hemSuperClient.subscribe(update)


#Define your power threshold in 5
#In this app, the threshold is 5W
POWER_THRESHOLD = 50

#Define the load node numbers
loadNodes = [0, 1, 2, 3]



#This is your app intelligence. You can define more functions like this 
# to handle the logic and intelligence part of your app
def onlineDemandManagement(powerData):

	#print powerData

	#You should know which nodes are loads, source and others
	#Here you have to sum up the power for all 

	#1.iterate through the power values for the nodes
	#2. add the load power values and see if crosses a threshold
	#3. You should know which nodes are loads and what is there threshold

	#For this app, nodes 1,2,3,4 are loads
	#This is defined in the variable 'loadNodes' at the beginning of the app
	totalLoadPower = 0
	for i, val in enumerate(powerData):

		#This if checks if the nodes you're iterating through belongs 
		#to one of the load nodes
		if(i in loadNodes):
			totalLoadPower = totalLoadPower + float(val)
			#val is string. You should convert that to float by doing float(vals)

	#The power threshold is set using the variable POWER_THRESHOLD at the 
	#beginning of the app
	#If the value is greater than threshold, turn off non critical load
	time.sleep(5)
	if(totalLoadPower > POWER_THRESHOLD):
		print totalLoadPower
		hemSuperClient.sendRequest("/api/turnoff/2")
		
def main():
	
	#Turning on a bunch of loads
	hemSuperClient.sendRequest("/api/turnon/0")
	time.sleep(5)
	hemSuperClient.sendRequest("/api/turnon/2")

	#Use this to send a request to server to pull power data for all nodes
	#Get this data every 3 seconds
	while True:
		hemSuperClient.sendRequest("/api/getacpoweractive/all")
		#get the data from server every 10 secs. 
		time.sleep(10)

if __name__ == "__main__":
    main() 

